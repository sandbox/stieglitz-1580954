
Slider module for Drupal 7 creates a scrollable jQuery node display effect called the "coda slider", which was made famous by the site:

http://www.panic.com/coda/

Each panel of the Slider is an individual node. You can have any node you like appear inside a slider.

Version 7.1.x usage
------------------

This version of slider is designed for Drupal 7. Since CCK is now in Drupal 7 core, there is no need to worry about CCK. Think of a Slider as an extension of the standard CCK nodereference field. You first make a bunch of nodes that will appear as individual slides. Then, make a node type that will be the actual sliders themselves. An example site structure may look something like this:

- Meet the team (node of type 'Slider', with nodereference field linking to the nodes below)
--- Darren (node of type 'User bio')
--- Sam (node of type 'User bio')
--- Support staff (node of type 'Page')
--- Other staff (node of type 'Page')

How to set it up:

1. Install and enable Slider and the node_reference module within the references module.
2. Go to admin/content/types and add a content type that will be the actual sliders themselves. E.g. call it 'slider'.
3. Click on 'Manage fields' and add a new node_reference field. Call it whatever you like. Use the select widget. Make sure 'unlimited' is chosen for 'Number of values'. Allow it to reference the nodes that will be slides within the slider.
4. Go to 'Display fields' and from the drop-down boxes on your new field, choose either "Slider (tabs above)" or "Slider (tabs below)" under the teaser and full node settings.
5. Create a new node of this slider content type, select the slides you want in the nodereference field, and save!
6. You can re-order fields, remove the label and do all other CCK tweaks the usual way.

There is no support for having more than one slider on a page, and it's not possible to re-order the nodes, they are ordered by title. There are a few modules that claim to provide a widget for ordering nodereference fields, but I don't support that. Try http://drupal.org/node/321486.

If you need to change the width and height or any other properties of the slider, you can do so in your theme's CSS file (overriding the styles in this module's css/slider.css).

Credits
-----------

This module was written by Mark Theunissen for Digital People Online, a London based Digital Agency that specialises in Drupal development.
It was modified for Drupal 7 by Jon Stieglitz for Lansing Response Action Web Designers (rawdesigners.com)
It uses several jQuery plugins: scrollTo, localScroll, serialScroll.

The jQuery code in slider.js was created by Remy Sharp and is explained in detail on his blog at: http://jqueryfordesigners.com/coda-slider-effect/
