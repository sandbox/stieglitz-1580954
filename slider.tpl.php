<?php 

/**
 * @file slider.tpl.php
 * Default output for a slider node.
 */
?>
<div id="sliderwrapper">
  <div id="slider">
    <?php if (!$tabs_above): ?>
      <div class="scroll">
        <div class="scrollContainer">
          <?php print $slider7_content_formatted ?>
        </div>
      </div>
      <?php print $tabs_formatted ?>
    <?php else: ?>
      <?php print $tabs_formatted ?>
      <div class="scroll">
        <div class="scrollContainer">
          <?php print $slider7_content_formatted ?>
        </div>
      </div>
    <?php endif; ?>
    <div class="clear-block"></div>
  </div>
  <script type="text/javascript">
    <!--//--><![CDATA[//><!--
    //Hide while Slider is loading
      $('div.scrollContainer').css('display', 'none');
    //--><!]]>
  </script>
</div>
